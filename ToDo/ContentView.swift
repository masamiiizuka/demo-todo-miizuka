//
//  ContentView.swift
//  ToDo
//
//  Created by miizuka on 2021/11/29.
//

import SwiftUI

struct ContentView: View {
    
    @State var newItem: String = ""
    @State var toDoList: [String] = ["買い物", "洗濯"]
    @State var isAlert: Bool = false
    
    var body: some View {
        VStack {
            HStack {
                Text("新しい予定の追加").font(.largeTitle).padding(.leading)
                
                Spacer()
            }
            
            HStack {
                TextField("新しい予定を入力してください", text: $newItem)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .frame(width: 300)
                
                Button(action: {
                    if self.newItem == "" {
                        self.isAlert.toggle()
                    } else {
                        self.toDoList.append(self.newItem)
                        self.newItem = ""
                        UserDefaults.standard.set(self.toDoList, forKey: "ToDoList")
                    }
                }) {
                    ZStack {
                        RoundedRectangle(cornerRadius: 5)
                            .frame(width: 50, height: 30)
                            .foregroundColor(.green)
                        
                        Text("追加").foregroundColor(.white)
                    }
                }
            }
                                
            HStack {
                Text("To Do List").font(.largeTitle).padding(.leading)
                
                Spacer()
                
                EditButton().padding()
            }
            
            List {
                ForEach (toDoList, id: \.self) { item in
                        Text(item)
                }.onDelete { (IndexSet) in
                    self.toDoList.remove(atOffsets: IndexSet)
                    UserDefaults.standard.set(self.toDoList, forKey: "ToDoList")
                }.onMove { (IndexSet, Destination) in
                    self.toDoList.move(fromOffsets: IndexSet, toOffset: Destination)
                    UserDefaults.standard.set(self.toDoList, forKey: "ToDoList")
                }
            }
            
            Spacer()
        }.alert(isPresented: self.$isAlert) {
            Alert(title: Text("予定を入力してください"), dismissButton: .default(Text("ok")))
        }.onAppear() {
            guard let defaultItem = UserDefaults.standard.array(forKey: "ToDoList") as? [String] else
                {return}
            self.toDoList = defaultItem
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

